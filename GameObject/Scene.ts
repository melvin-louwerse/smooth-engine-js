import GameObject from "./GameObject";
import {GameState} from "../State";

export class Scene {

  private _objects: [GameObject];

  private _state: GameState;

  constructor() {
    this._objects = [];
  }

  private addChild(child: GameObject) {
    this._objects.push(child);
  }


  animate(state: GameState): void {
    this._state = state;
  }

  update(state: GameState): void {

  }

  render() {
  }

  getGameObjects(): [GameObject] {
    return this._objects;
  }
}