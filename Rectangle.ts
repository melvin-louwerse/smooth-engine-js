import { CustomPIXIComponent } from "react-pixi-fiber";
import * as PIXI from "pixi.js";

const TYPE = "Rectangle";
export const behavior = {
    customDisplayObject: props => new PIXI.Graphics(),
    customApplyProps: function(instance, oldProps, newProps) {
        const { line, x, y, width, height } = newProps;
        instance.clear();
        instance.lineStyle(1, line);

        instance.drawRect(x, y, width, height);
        // instance.endFill();
    }
};
export default CustomPIXIComponent(behavior, TYPE);