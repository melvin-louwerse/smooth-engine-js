import {CollisionTree} from "./CollisionTree";
import {Bounds} from "../ValueObject/Bounds";
import RigidBody from "../RigidBody";
import {CollisionObject} from "./CollisionObjects";

export namespace CollisionRegistry {
  let tree:number = null;

  export function getCollsionTree(): CollisionTree {
    if (typeof this.tree === "undefined") {
      this.tree = new CollisionTree(10, 5, {x: 0, y: 0, w: window.innerWidth, h: window.innerHeight} as Bounds, 0);
    }
    return this.tree;
  }

  export function hasCollision(body:CollisionObject) {
    let tree = getCollsionTree();
    tree.getObjectsInQuadrant(body)
  }
}