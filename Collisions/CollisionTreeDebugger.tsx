import {CollisionRegistry} from "./CollisionRegistry";
import {CollisionTree} from "./CollisionTree";

import * as React from "react";
import * as PixiFibre from "react-pixi-fiber"
import Rectangle from "../Rectangle"

const {Container} = PixiFibre;
const {Component} = React;

export class CollissionTreeDebugger extends Component {
  private colTree: CollisionTree;

  constructor(...params) {
    super(...params);

    // ...start it a bit higher to account for motion
    this.y = this.props.y - 15;

    // ...increase direction to speed up
    this.direction = 1.5;

    // ...increase limit to move more
    this.limit = 30;
    const {x, y, w, h} = this.props;

    this.colTree = CollisionRegistry.getCollsionTree();
  }


  animate(state) {
  }

  render() {

    let table = this.mainNodes();
    console.log(table);
    return (
      <Container>
        {table}
      </Container>
    );
  }


  mainNodes() {

    console.log(this.colTree);
    let nodes = [];

    nodes.push(<Rectangle key={nodes.length + 1}  x={this.colTree.treeSize.x} y={this.colTree.treeSize.y} width={this.colTree.treeSize.w} height={this.colTree.treeSize.h} line={0xFFFF00}/>);


    console.log(nodes.length);
    return this.renderSubNodes(this.colTree.treeNode, nodes);
  }

  private renderSubNodes(tree: CollisionTreeNode, nodes: any) {
    tree.nodes.forEach(node => {

        nodes.push(<Rectangle key={nodes.length + 1} x={node.treeSize.x} y={node.treeSize.y} width={node.treeSize.w} height={node.treeSize.h} line={0xFFFF00}/>);

        if (node.nodes.length > 0) {
          nodes = this.renderSubNodes(node, nodes);
        }
      }
    );
    return nodes;
  }
}


