import * as React from "react";
import {GameState} from "./State";
import {Vector2} from "./ValueObject/Vector2";
import {CollisionRegistry} from "./Collisions/CollisionRegistry";
import GameObject from "./GameObject/GameObject";

const {Component} = React;

export default class StaticBody extends Component implements GameObject {
  private collisonRegistry: CollisionRegistry;
  get y(): number {
    return this._y;
  }
  get x(): number {
    return this._x;
  }
  protected _x: number;
  protected _y: number;
  private state: Vector2;
  private props: any;

  constructor(...params) {
    super(...params);

    this._x = this.props.x;
    this._y = this.props.y;
    this.state = {
      x: this.props.x,
      y: this.props.y,
    };

    this.collisonRegistry;
  }

  componentWillReceiveProps(props) {
    this._x = this.props.x;
    this._y = this.props.y;
  }

  animate(state: GameState): void {
  }


  render(): any {
    console.log('Implement your own render');
  }
}
