import * as React from "react";
import * as ReactDOM from "react-dom";
import Game from "./Game";

const {render} = ReactDOM;

render(
    <Game
        w={window.innerWidth}
        h={window.innerHeight}
    />,
    document.querySelector(".app"),
);
